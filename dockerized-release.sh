#!/bin/bash
LRE="docker run -i -t --rm -v $(pwd):/data -e LIMBO_DATASET_GRAPH=$LIMBO_DATASET_GRAPH -e LIMBO_DATASET_FILE=$LIMBO_DATASET_FILE  aksw/limbo-base"
$LRE 'make release-start'
$LRE 'make release-finish'
$LRE 'make deploy-dcat'
