#!/bin/bash
INIT_VERSION=0.1
USER=`whoami`
datasets=`cat $1 | jq -c -r '.[]'`
catalog_dir=target/metadata-catalog
username=$(head -n 1 $2)
pw=$(tail -n 1 $2)
echo "$datasets" | while read dataset; do
	LIMBO_DATASET_NAME=`echo $dataset | jq -r '.name'`
	LIMBO_DATASET_GRAPH=`echo $dataset | jq -r '.graph'`
	echo $LIMBO_DATASET_GRAPH
	LIMBO_DATASET_WORKFLOW=`echo $dataset | jq -r '.workflow'`
	LIMBO_DATASET_FILE=$LIMBO_DATASET_NAME.ttl
	# LIMBO runtime environment
	LRE="docker run -i --rm \
		-v $(pwd)/$LIMBO_DATASET_NAME:/data \
		-e LIMBO_DATASET_GRAPH=$LIMBO_DATASET_GRAPH \
		-e LIMBO_DATASET_FILE=$LIMBO_DATASET_FILE \
		-e LIMBO_DATASET_WORKFLOW=$LIMBO_DATASET_WORKFLOW aksw/limbo-base"
	# Is there a dir with this dataset name in the local file system?
    if [ ! -d $LIMBO_DATASET_NAME ]; then 
		# If there is no dir with the dataset name, check if there is a remote repo
		repo_exists=$(git ls-remote https://$username:$pw@gitlab.com/limbo-project/$LIMBO_DATASET_NAME.git)
		if  [ "$repo_exists" != "" ]; then
			# If there is a remote repo, clone it
			git clone https://$username:$pw@gitlab.com/limbo-project/$LIMBO_DATASET_NAME.git
			cd $LIMBO_DATASET_NAME
			git config user.name `git config --global user.name`
			git config user.email `git config --global user.email`
			git update-index --skip-worktree .gitmodules
			git config --file=.gitmodules submodule.dataset-includes.url https://gitlab.com/limbo-project/dataset-includes.git
			# git submodule init
			# git submodule update --recursive
			# git submodule foreach git pull origin master
        	git submodule sync --recursive
 			git submodule update --init --remote --recursive
			git remote set-url origin https://$username:$pw@gitlab.com/limbo-project/$LIMBO_DATASET_NAME.git
		else 
			# If there is not a remote repo, create it from scratch
			mkdir $LIMBO_DATASET_NAME
			cd $LIMBO_DATASET_NAME
			git init
			git config user.name `git config --global user.name`
			git config user.email `git config --global user.email`
			sh -c "$(curl -fsSL https://gitlab.com/limbo-project/dataset-includes/raw/master/init-dataset-repo.sh)"
			sed -i 's/{version-number}/'"${INIT_VERSION}"'/' config.sh 
			# git submodule update --recursive
			# git submodule foreach git pull origin master
			git submodule sync --recursive
   			git submodule update --init --remote --recursive
			git remote add origin https://$username:$pw@gitlab.com/limbo-project/$LIMBO_DATASET_NAME.git
			git checkout master 2>/dev/null || git checkout -b master
			git push -u origin master
			git add . && git commit -m 'set up env' 
			git push origin master
		fi
	else 
		# If there is a dir with the dataset name, update it, provide credentials for automatic processing without prompting
		echo "Directory already exists!" 
		cd $LIMBO_DATASET_NAME
		if [ -d .git ]; then
			git pull origin master 
			# git submodule foreach git pull origin master 
			git submodule sync --recursive
   			git submodule update --init --remote --recursive
			git config user.name `git config --global user.name`
			git config user.email `git config --global user.email`
			git remote set-url origin https://$username:$pw@gitlab.com/limbo-project/$LIMBO_DATASET_NAME.git
		else 
			echo "Exiting! This is not a git repo."
			exit 1
		fi
  	fi
	echo "before metadata-catalog"
	# Clone the metadata catalog if its not there 
	[ -d $catalog_dir ] && (cd $catalog_dir && git pull origin master) || git clone https://$username:$pw@gitlab.com/limbo-project/metadata-catalog.git $catalog_dir
	(cd $catalog_dir && git config user.name `git config --global user.name` && git config user.email `git config --global user.email`)
	[ -z $LIMBO_DATASET_WORKFLOW ] || $LRE 'make lift-data' > /dev/null
	$LRE 'make release-start' > /dev/null
	$LRE 'make release-finish' > /dev/null
	$LRE 'make deploy-dcat' > /dev/null
	# Give back rights to the current user for files that have been created in the LIMBO (docker) environment
	sudo chown -R "${USER:-$(id -un)}" . 
	# Continue with the next dataset
	cd ../
	# Wait a little in order to avoid merge conflicts!
	sleep 30
done 

