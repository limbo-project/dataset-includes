#!/bin/sh

# 'running', 'success' or 'failure' is in this file
STATUS=`cat $CI_JOB_NAME.txt`

[ -d public ] || mkdir public
# Set values for shields.io fields based on STATUS
if [ $STATUS = "failure" ]; then
	BADGE_SUBJECT="failed"
	BADGE_COLOR="red"
elif [ $STATUS = "success" ]; then
	BADGE_SUBJECT="passed"
	BADGE_COLOR="brightgreen"
else
	exit 1
fi

# Get the badge from shields.io
echo "INFO: Fetching badge $SHIELDS_IO_NAME from shields.io to $BADGE_FILENAME."
SHIELDS_IO_NAME=$CI_JOB_NAME-$BADGE_SUBJECT-$BADGE_COLOR.svg
curl "https://img.shields.io/badge/$SHIELDS_IO_NAME" > public/$CI_JOB_NAME.svg

