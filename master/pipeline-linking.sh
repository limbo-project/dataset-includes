#!/bin/bash
mkdir -p $LINKING_DIR
# clone the linkset repo into the project folder or update it in case it is already there
(if cd $LINKING_LINKSETS_DIR; then git pull; else git clone $LINKING_LINKSETS_GIT $LINKING_LINKSETS_DIR; fi)
# clone the link assets repo into the project folder or update it in case it is already there
# (if cd $LINKING_ASSETS_DIR; then git pull; else git clone $LINKING_ASSETS_GIT $LINKING_ASSETS_DIR; fi)
# If this procedure is executed from the metadata-catalog; there is no need to pull the catalog repository
if [ "$(basename "$(pwd)")" != `echo "$(basename $CATALOG_DIR)"` ]; then
	(if cd $CATALOG_DIR; then git pull; else git clone $CATALOG_GIT $CATALOG_DIR; fi)
fi
mkdir -p $LINKING_SPECS
# Get the linking tasks
linktasks=`sparql-integrate --jq $PREFIXES_RDF $CATALOG_FILE $SELF/linking-rules/linkingtask-variables.sparql | jq -c -r '.[] | .id + " " + .url.id + " " + .symmetric + " " + .identifier + " " + .prefix + " " + .variables'`
echo "$linktasks" | while read linktask; do
    export LINKING_TASK=`echo $linktask | cut -d ' ' -f 1`
	export LINKING_TASK_SYMMETRIC=`echo $linktask | cut -d ' ' -f 3`
	export LINKING_TASK_IDENTIFIER=`echo $linktask | cut -d ' ' -f 4`
	export LINKING_TASK_PREFIX=`echo $linktask | cut -d ' ' -f 5`
	variablestring=`echo $linktask | cut -d ' ' -f 6-`
	delimiter="<-...->"
	regexDelimiter='(.*)<-===->(.*)'
	s=$variablestring$delimiter
	while [[ $s ]]; do
		variable="${s%%"$delimiter"*}"
		s=${s#*"$delimiter"}
		[[ $variable =~ $regexDelimiter ]] && export "LS_TEMPLATE_VAR__${BASH_REMATCH[1]}"="${BASH_REMATCH[2]}"
	done
	curl `echo $linktask | cut -d ' ' -f 2` > "${LINKING_DIR}/template.sparql"
	# Get the linking task instances
	instances=`sparql-integrate --jq $PREFIXES_RDF $CATALOG_FILE $SELF/linking-rules/linkingtask-instances-only-new.sparql | jq -c -r '.[] | "\(.output) \(.version)"'`
	echo "$instances" | while read instance; do
		ds1=`echo $instance | cut -d ' ' -f 1`
		ds2=`echo $instance | cut -d ' ' -f 5`
		ds1Id=`echo $instance | cut -d ' ' -f 2`
		ds2Id=`echo $instance | cut -d ' ' -f 6`
		artifact1=`echo $instance | cut -d ' ' -f 3`
		artifact2=`echo $instance | cut -d ' ' -f 7`
		version1=`echo $instance | cut -d ' ' -f 4`
		version2=`echo $instance | cut -d ' ' -f 8`
		linksetVersion=`echo $instance | cut -d ' ' -f 9`
		URLs1=`DATASET_ID=$ds1 sparql-integrate --jq $PREFIXES_RDF $CATALOG_FILE $SELF/linking-rules/download-URL.sparql | jq -c -r '.[].downloadURL.id'`
		URLs2=`DATASET_ID=$ds2 sparql-integrate --jq $PREFIXES_RDF $CATALOG_FILE $SELF/linking-rules/download-URL.sparql | jq -c -r '.[].downloadURL.id'`
		linktaskFolderName="$LINKING_TASK_PREFIX-$artifact1-TO-$artifact2"
		linksetName="$LINKING_TASK_PREFIX-$artifact1-$version1-TO-$artifact2-$version2"
		RESULTS="$LINKING_LINKSETS_DIR/$linktaskFolderName/"
		echo "$URLs1" | while read URL1; do
			export SOURCE_URL="${URL1}"
			# x1=`echo "${URL1}" | cut -d '/' -f 9 | cut -d '.' -f 1 `
			echo "$URLs2" | while read URL2; do
				export TARGET_URL="${URL2}"
				# x2=`echo "$URL2" | cut -d '/' -f 9 | cut -d '.' -f 1`
				LS_PATH="$LINKING_SPECS/ls-$linksetName.ttl"
            	sparql-integrate "${LINKING_DIR}/template.sparql" --w=trig/pretty > "$LS_PATH"
				fin=0
				rid=`curl -F config_file=@"$LS_PATH" "$LINKING_HOST/submit" 2>/dev/null | jq '.requestId' | cut -d '"' -f 2`
				while [[ fin -le 1 ]]; do
					fin=`curl "$LINKING_HOST/status/$rid" 2>/dev/null | jq .status.code`
					sleep 0.5;
				done
				curl "$LINKING_HOST/result/$rid/accept.nt" 2>/dev/null > tmpres
				size=`wc -l <tmpres | bc`
				if [[ $size -ge 1 ]]; then
					mkdir -p $RESULTS
					cp tmpres "$RESULTS/$linksetName.nt"
	 				echo "SOURCE_ID=$ds1Id\nTARGET_ID=$ds2Id\nVERSION=$linksetVersion\nTASK_ID=$LINKING_TASK_IDENTIFIER" > "$RESULTS/config.sh"
					echo 'include ../dataset-includes/master/Makefile' > "$RESULTS/Makefile"
					rm tmpres
				fi
			done
		done
	done
done
