## rdfq.sh - Tool for querying RDF in a Bash interoperable way

# Rough idea what it could look like
# RDFQ_PREFIXES='prefixes.ttl' 
# RDFQ_MODEL='rdf.ttl'

# rdfq.sh '?x a dcatDataset'
# rdfq.sh 'SELECT ?s ?o { ?s rdfs:label ?o }'


# In a second phase, we could reuse or create an XPath-like DSL for making queries shorter than plain SPARQL
# Labels af all datasets
# rdfq.sh '{ [] a dcatDataset } | rdfs:label'

