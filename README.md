# 1. Setting up LIMBO dataset repositories

## 1.1 Set up a new dataset repository
1. Create a new .git repository under the namespace of the [Limbo Group on gitlab](https://gitlab.com/limbo-project)
2. Name your dataset with the LIMBO-ID. The LIMBO-ID can be taken from the Table "Raw Data - Mapping Status" in our [Confluence](https://confluence.brox.de/display/LIMBO/Raw+Data+-+Mapping+Status), e.g. **train_2-dataset**
3. Initialize the repository by issuing the first commit on Gitlab
4. Clone the repository into your local workspace, e.g. `git clone https://gitlab.com/limbo-project/train_2-dataset.git`
5. In case, you do not have installed `curl` on your local system, type: 
```bash
apt update && apt-get upgrade -y && apt-get install -y curl
```
6. Enter the directory of the dataset repository in your local workspace, e.g. `cd train_2-dataset`:
* Either: Issue `export VERSION=1.0 && sh -c "$(curl -fsSL https://gitlab.com/limbo-project/dataset-includes/raw/master/automatic-setup.sh)"` for automatic setup
* Or: follow steps 7-10 for manual setup
7. Set up the repository environment for dataset releases with:
```bash
sh -c "$(curl -fsSL https://gitlab.com/limbo-project/dataset-includes/raw/master/init-dataset-repo.sh)"
```
The following files are generated:
* **.gitmodules**: file which registers dataset-includes as a submodule
* **dataset-includes**: folder with build tools
* **.gitlab.ci.yml**: file to trigger automatic tests for datasets
* **config.sh**: configuration file to store environment variables. 
* **Makefile**: file that references the master-Makefile from the dataset-includes directory

8. Upon having created the `config.sh` file, fill in the placeholders (marked by braces `{}`) with your specific dataset parameters. Note that the `BASE_IRI` represents the last part of the IRI which describes the distribution ([metadata-catalog](https://gitlab.com/limbo-project/metadata-catalog/raw/master/catalog.all.ttl)). The first part of the IRI is the LIMBO metadata namespace (`mcloud: <https://metadata.limbo-project.org/>`), which is automatically appended during the release process. 

```bash
# REQUIRED PARAMETERS
# The ID of the Limbo dataset. In case it is not provided, it is automatically determined from the repository name
LIMBO_DATASET_ID={LIMBO-ID}
# The name of the Limbo dataset (LIMBO_DATASET_ID of the Table Raw Data in Confluence), e.g. *RNI Bahnsteigdaten*
LIMBO_DATASET_NAME={dataset-name} 
# The version of the Limbo dataset, e.g *0.1*
VERSION={version-number}
# OPTIONAL PARAMETERS
# The mCLOUD dataset distribution that the LIMBO version is based on, e.g., *distribution-5670eb707309b5e1523b0d97fb0353ac* ([DB Stationsdaten RNI](download-data.deutschebahn.com/static/datasets/stationsdaten/DBSuS-Uebersicht_Bahnhoefe-Stand2018-03.csv)) . This parameter is mandatory for keyword indexing
BASE_IRI=distribution-{hash}
# The dataset license. This references a license IRI, e.g. *<http://dcat-ap.de/def/licenses/cc-by>*
LIMBO_DATASET_LICENSE={license-IRI}
```
9. Add your data files to the root of the repository. The data can be either raw `.ttl` files or `.zip` folders containing the raw files. In case your data is in another RDF format (e.g., rdfxml, ntriples),
install [raptor2-utils](https://stedolan.github.io/jq/) and transform the data (e.g., `rapper -i ntriples -o turtle train_2-dataset.nt > train_2-dataset`). Delete the data files that are not in turtle format.
10. Add all changes to the staging area and commit your changes (`git add . && git commit -m 'set up env' && git push`)

## 1.2 Set up a new linkset repository

1. If you have not yet done so, clone the LIMBO repository for linkset releases, e.g. `git clone https://gitlab.com/limbo-project/limbo-linksets.git`
2. Enter the linkset repository (`cd limbo-linksets`) 
3. Choose a linkset name and export it as the `LINKSET` environment variable, e.g. `export LINKSET=test-linkset`
4. Set up you linkset folder with this command:

```bash
sh -c "$(curl -fsSL https://gitlab.com/limbo-project/dataset-includes/raw/master/init-linkset-repo.sh)"
```
The following items are generated:
* **LINKSET-FOLDER**: folder from which the release process for your specific linkset is triggered
* **config.sh**: configuration file to store linkset specific environment variables. 
* **Makefile**: file that references the master-Makefile from the dataset-includes directory

5. Go to your linkset folder, e.g. `cd test-linkset`
6. Customize the config file with the parameters that are specific for your linkset

Template for the config.sh (LINKSET RELEASES)

```bash
## REQUIRED parameters ###
# The dct:identifier of the source dataset (usually a Limbo dataset) from which links point to another dataset in the LOD or LIMBO cloud, e.g. *org.limbo:carsharing-koeln:0.1*
SOURCE_ID={SOURCE-DATASET-ID}
# The ID of the target dataset (a Limbo dataset or a LOD dataset) to which the source dataset points to, e.g. *org.limbo:ampeln-koeln:0.1*
TARGET_ID={TARGET-DATASET-ID}
# The version of the Limbo dataset
VERSION={VERSION-NUMBER}
### OPTIONAL parameters ###
# The dct:identifier of the linking task that led to the creation of the linkset, e.g. *mcloud:linktask-org.limbo-org.limbo-ampeln-koeln-TO-org.limbo-ampeln-koeln-0*
TASK_ID={TASK_ID}
```

## 1.3 Update an existing (remote) repository
In case, the dataset already resides on gitlab alongside the required supplementary material (`config.sh`, `.gitmodules`, `dataset-includes`, `.gitignore`), clone the existing repository into your local workspace and issue the following commands in your terminal:

```bash
# Remove .gitmodules from the working tree index, since the dataset-includes path for the local repo is different from the remote path (https://codeclimber.net.nz/archive/2016/12/19/ignoring-files-on-git-but-just-for-a-while-with-assume-unchanged/)
git update-index --skip-worktree .gitmodules
# Add the remote Gitlab update url to your .gitmodules file. This only takes effect in your local workspace
git config --file=.gitmodules submodule.dataset-includes.url https://gitlab.com/limbo-project/dataset-includes.git
# Initialize the submodule in your local workspace
git submodule init
# Reads submodule files from the remote repository. Note that this only reads the dataset-includes version that is referenced by the remote repo. It points to a certain commit hash of the submodule repo, which might be outdated. To update the local repo to point to the latest submodule commit hash, go to 3. "Updating submodules"
git submodule update --recursive
```
Make your changes in the data files or add new files in the root of the repository.

# 2. Update submodule(s)

Once the Makefile has been set up, you can conveniently update the dataset-includes submodules using
```bash
make git-submodule-update
# which runs
# git submodule foreach git pull origin master
```
This fetches the files from the current version (commit hash) of the submodule as it is referenced in the remote repository. In case, you need to obtain the latest version of the submodule you need to pull the changes in the submodule directory and commit/push this update in the current repository.

# 3. Release LIMBO data

## 3.1 Runtime environment and automatic release 
The following command sets up the runtime environment for the release.  

`make setup` 

The command triggers installation of the following types of applications: 
* Utility tools: e.g. [jq](https://stedolan.github.io/jq/), [raptor2-utils](https://stedolan.github.io/jq/), or [cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)
* SPARQL-based command-line data transformations: [SparqlIntegrate](https://github.com/SmartDataAnalytics/SparqlIntegrate)

Alternatively, you can pull and run the LIMBO docker image: 

```bash
docker run -i -t --rm aksw/limbo-base:latest
```
whenever you need to run commands from the LIMBO pipeline. For futher details, see the [LIMBO docker repo](https://gitlab.com/limbo-project/limbo-docker). 

Using the LIMBO docker image, you can also start an automatic release process for your dataset. 

```bash
sh -c "$(curl -fsSL https://gitlab.com/limbo-project/dataset-includes/raw/master/lift-n-release.sh | bash -s <path-to-datasets-json-file> <path-to-credentials-file>)"
```

datasets.json (Example)
```json
[ 
    { "name":"call-a-bike-test2", "graph":"https://portal.limbo-project.org/call_a_bike/", "workflow":""}
]
```
credentials (Example)
```bash
<your-Gitlab-username>
<your-Gitlab-password>
```

## 3.2 Index your dataset with keywords (optional) 
The dataset can be annotated with subject descriptors from the German Integrated Authority File (GND) by utilizing the command-line tool [Kindex](https://gitlab.com/limbo-project/keyword-indexing). The German National Library utilizes the GND to index publications. Hence, enhancing you dataset with GND subject descriptors helps to identify related material. 

`make keywords`

Note that, in order to make keyword indexing work, you need to state the corresponding `BASE_IRI` of the mCLOUD distribution which can be determined from the [metadata-catalog](https://gitlab.com/limbo-project/metadata-catalog/raw/master/catalog.all.ttl). This is because the indexing process relies on a text-based dataset description which is usually provided for [mCLOUD](https://www.mcloud.de/) datasets. Once you started the indexing process and the KINDEX tool identifies subject descriptors that fit your dataset description, you will be asked if the respective keyword is relevant for the dataset. State 'y' (yes) for relevant descriptors and 'n' (no) for irrelevant descriptors. 

![alt text](https://gitlab.com/limbo-project/keyword-indexing/raw/master/img/cli-output.png "Terminal output")

## 3.3 Test your data  (optional)
In order to test your data in regard to schema alignment, use RDFUnit-based testing by issuing the command listed below. This command installs the [RDFUnit](https://github.com/AKSW/RDFUnit) test software for RDF data in your target folder, in case it is not yet there. 

`make rdfunit`

In addition to schema testing, the build tools can also test the syntactic and semantic consistency of the data through a webservice of the [CISS Quality Assurance Center](https://www.ciss.de/). For instance, it is checked whether places are located within Germany or ZIP codes are formatted correctly. To perform testing based on the CISS Quality Assurance Center, type:

`make ciss`

Currently, testing is only applied to dataset releases, whereas if you issue one of the above listed test commands with a linkset release `config.sh`, the test procedures are not executed.

Whenever you make changes in the raw `.ttl` data, the `.gitlab-ci.yml` file triggers a test workflow which consists of RDFUnit schema tests und CISS quality tests. You can also add bagdes to your project to see at first glance whether your data conforms to the quality requirements. 

![alt text](https://gitlab.com/limbo-project/dataset-includes/raw/master/img/Badges.png "Project Badges")

Fill in the following URIs for project-level badges

## RDFUnit Bagde

* Link: `https://gitlab.com/%{project_path}/-/jobs/artifacts/master/raw/public/rdfunit.ttl?job=rdfunit`
* Badge Image URL: `https://gitlab.com/%{project_path}/-/jobs/artifacts/master/raw/public/rdfunit.svg?job=rdfunit`

## CISS Badge

* Link: `https://gitlab.com/%{project_path}/-/jobs/artifacts/master/raw/public/ciss.trig?job=ciss`
* Badge Image URL: `https://gitlab.com/%{project_path}/-/jobs/artifacts/master/raw/public/ciss.svg?job=ciss`

## Pipeline Badge

* Link: `https://gitlab.com/%{project_path}/pipelines`
* Badge Image URL: `https://gitlab.com/%{project_path}/badges/master/pipeline.svg`

## 3.4 Release your data
In order to release a dataset, execute the following command:

`make release-start`

In case nothing goes wrong, type the command listed below. This pushes the release dataset to the master branch taking the version number (as stated in `config.sh`) as the release tag 

`make release-finish`

## 3.5 Rollback the release
In case something goes wrong during this process, the operations that have been performed after issuing the release  commands (e.g., creating and checking out a temporary release branch), they can be reverted with: 

`make release-rollback`

Preferably, the command is succeeded by a workspace clean-up (`make clean`). You might also want to make sure that the version counter is set back to its initial number (i.e., the number before the failed release operations had been started) as `make release-start` automatically increments the version version number in the `config.sh` file. Additionally, you should make sure that the release tag for your current dataset version does not yet exist prior to issuing the next release-start command. Issue 

* `git tag` (to inspect current tags)
* `git tag -d v{version}` (to delete a prematurely created tag in case there exists one) 

## 3.6 Publish released data 
Publishes the metadata of the dataset in the metadata-catalog. The `.gitlab-ci.yml` from the [metadata-catalog repository](https://gitlab.com/limbo-project/metadata-catalog) will be executed thus making sure that the metadata of the newly released dataset is syntactically correct and correctly added to the catalog. Please do not delete files from the `toLoad` directory as this would cause data loss in the catalog.

`make deploy-dcat`

## 3.7 Visualize LIMBO data as a data cloud picture (optional)
All Limbo datasets that are listed in the [metadata-catalog](https://gitlab.com/limbo-project/metadata-catalog/raw/master/catalog.all.ttl) can be viualized in LOD cloud pictures. The drawing procedure
utilizes the [lod-cloud-draw](https://github.com/lod-cloud/lod-cloud-draw) application. You can either depict the LIMBO datasets with:

`make draw-small`

or show the entire LOD cloud in combination with the LIMBO datasets:

`make draw-large`

## 3.8 Clean up your workspace
In order to clean up your workspace, issue the following command: 

`make clean` 

This removes the `target` folder which contains all files and folders that have been generated through execeution of the `make` commands, i.e. the release process and its preparational phases. The target folder contain some, if not all of the below listed elements: 

* **dcat.ttl**: initial file to store DCAT metadata
* **effective.dcat.ttl**: DCAT metadata file with local download URLs and and a blank node dataset URL
* **release.dcat.ttl**: the final DCAT file (containing dataset URLs and links for `http:` or `https:` download links) which will be deployed to the [metadata-catalog](https://gitlab.com/limbo-project/metadata-catalog/raw/master/catalog.all.ttl) 
* **data**: folder which contains the pre-processed `*.ttl` files that are to be released
* **keyword-indexing**: repository with the KINDEX command line tool and potentially identified keywords for the dataset
* **metadata-catalog**: repsotiory with the LIMBO metadata-catalog
* **qac**:folder with the test results of the [CISS Quality Assurance Center](https://www.ciss.de/) for your dataset
* **RDFUnit**: repository of the [RDFUnit](https://github.com/AKSW/RDFUnit) software with the generated `rdfunit-validate .jar` file that performs schema testing
* **results**: folder with RDFUnit test results

After the removal of the `target` folder, the release process can be started from a clean workspace. This might come in handy in situations in which you had to cancel the release process prematurely. 

# 4 Update the metadata catalog

The [DCAT metadata](https://www.w3.org/TR/vocab-dcat-2/) for most of the raw datasets (from which LIMBO datasets have been derived) can be crawled from the mCLOUD portal. Dataset and distribution IRIs are published under the LIMBO namespace. In order to generate mCLOUD metadata, issue the following command: 

`make mcloud-catalog`

This generates a catalog file which can be commited and pushed to the LIMBO metadata-catalog repository with the request: 

`make mcloud-deploy`

# 5 Create implicit links

Link information (i.e., object URIs in LIMBO datasets linking to subject URIs in other LIMBO datasets) can be obtained with make commands from the branch `feature/implicit-links` of this repository. Firstly, an index has to be created. This index tracks for each subject URIs in which LIMBO dataset it occurs. Index creation is carried out with the in-memory database [Redis](https://redis.io/). After index creation, the redis dump is pushed to the [metadata-catalog repo](https://gitlab.com/limbo-project/metadata-catalog). The following command triggers indexing:

`make create-index`

In case, link information needs to be obtained for the entire metadata-catalog, i.e., all available LIMBO datasets, a global link information procedure can be triggered. 
From the redis index stored in the metadata-catalog the number of outgoing links (i.e., links from object URIs to subject URIs in different datasets) is determined for each LIMBO dataset  Based on this, RDF triples are created and pushed to the file [implicit-links-dcat.ttl](https://gitlab.com/limbo-project/metadata-catalog/-/blob/master/toLoad/implicit-links-dcat.ttl) in the metadata-catalog. Issue the following command: 

`make implicit-links`

The above mentioned procedure can also be carried out locally, i.e., only for a specific LIMBO dataset which is about to be released. Once, you are in the `feature/implicit-links` branch of this repository, implicit link information is automatically added during the release process, when issuing `make deploy-dcat`. The link information can then be found in the metadata file (named after the dct:identifier of the current dataset) and pushed to the `toLoad` folder of the metadata-catalog. 



