#!/bin/bash
# set up config.sh file
mkdir $LINKSET
cd $LINKSET
if [ ! -f config.sh ]; then
configSTR="SOURCE_ID={SOURCE-DATASET-ID}\n\
TARGET_ID={TARGET-DATASET-ID}\n\
SPEC_ID={LINK-SPEC-ID}
VERSION={VERSION-NUMBER}"
echo "$configSTR" > config.sh
fi
# setup Makefile
if [ ! -f Makefile ]; then
	echo "include ../dataset-includes/master/Makefile" > Makefile
fi



