#!/bin/bash
curl -fsSL https://gitlab.com/limbo-project/dataset-includes/raw/master/.gitmodules-template --output .gitmodules
git add .gitmodules && git commit -m 'add .gitmodules'
# remove .gitmodules from the working tree index, since the dataset-includes path for the local repo is different from the remote path
# https://codeclimber.net.nz/archive/2016/12/19/ignoring-files-on-git-but-just-for-a-while-with-assume-unchanged/
git update-index --skip-worktree .gitmodules
# initialize submodule
git submodule add https://gitlab.com/limbo-project/dataset-includes.git
git submodule init
# copy CI file to data repo
cp dataset-includes/.gitlab-ci-template.yml .gitlab-ci.yml
# set up config.sh file
if [ ! -f config.sh ]; then
configSTR="LIMBO_DATASET_ID=`basename $(pwd)`\n\
LIMBO_DATASET_NAME={dataset-name}\n\
VERSION={version-number}\n\
#BASE_IRI=distribution-{hash}\n\
#LIMBO_DATASET_LICENSE={license-IRI}"
echo "$configSTR" > config.sh
fi
# setup Makefile
if [ ! -f Makefile ]; then
	echo "include dataset-includes/master/Makefile" > Makefile
fi
# set up .gitignore with target folder inside
( FILE='.gitignore' LINE='target' ; grep -qsxF "$LINE" "$FILE" || echo "$LINE" >> "$FILE" )

