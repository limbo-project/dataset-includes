#!/bin/bash
# file where names of linkset dirs are listed
linksets=`cat $1 | jq -c -r '.[]'`
# path to the catalog repo
catalog_dir=target/metadata-catalog
# Git username
username=$(head -n 1 $2)
# Git password
pw=$(tail -n 1 $2)
# The name of the repo for all LIMBO linksets 
link_repo_root="limbo-linksets" 
# The path to the current working dir
link_repo=`echo $(pwd)`
# The name of the current working dir
link_repo_base=`basename $(pwd)`
# Make sure user name, email and password are set to allow for automatic commits
git config user.name `git config --global user.name`
git config user.email `git config --global user.email`
git remote set-url origin https://$username:$pw@gitlab.com/limbo-project/$link_repo_root.git
# In case the submodule dataset-includes is empty, update it 
if [ ! -n "$(ls -A dataset-includes)" ]; then 
	git update-index --skip-worktree .gitmodules 
	git config --file=.gitmodules submodule.dataset-includes.url https://gitlab.com/limbo-project/dataset-includes.git 
	git submodule init
	git submodule update --recursive
fi
# Make sure this is the most current dataset repo
git submodule foreach git pull origin master
# Check if the the current dir is the LIMBO linksets repo
if [ $link_repo_base == $link_repo_root ]; then 
	# Release each linkset separately
	#while read linkset || [[ -n $linkset ]]; do 
	echo "$linksets" | while read object; do
		linkset=`echo $object | jq -r '.name'`
		echo $linkset
		cd $linkset
		# Set the LIMBO runtime environment for the current linkset
		LRE="docker run -i --rm -v $link_repo:/data --env LINKSET=$linkset aksw/limbo-base" 
		# Clone the catalog dir in case it is not there, set credentials locally
		[ -d $catalog_dir ] && (cd $catalog_dir && git pull origin master) || git clone https://$username:$pw@gitlab.com/limbo-project/metadata-catalog.git $catalog_dir
		(cd $catalog_dir && git config user.name `git config --global user.name` && git config user.email `git config --global user.email`)
		# If config.sh is there, start the release procedure
		if [ -f config.sh ]; then 
			$LRE 'cd $LINKSET && make release-start' < /dev/null
			#$LRE 'cd $LINKSET && make release-finish' < /dev/null
			#$LRE 'cd $LINKSET && make deploy-dcat' < /dev/null
			sudo chown -R "${USER:-$(id -un)}" . 
			# Continue with the next linkset
		else 
			echo 'No config.sh provided. Please create it first!'
			exit 1
		fi
		cd ../ 
		# Wait a little in order to avoid merge conflicts!
		sleep 60
	done 
else 
	echo 'You are not in the right repository!'
	exit 1
fi

